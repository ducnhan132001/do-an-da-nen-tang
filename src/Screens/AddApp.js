import React, { useState, useEffect } from 'react';
import { COLORS, Images, FONTS, Icons } from "../Constants";
import { StyleSheet, Text, View, SafeAreaView, StatusBar, TouchableOpacity, Image, ScrollView, FlatList, TextInput } from 'react-native';
import axios from 'axios';
import RNFS from 'react-native-fs';
import RNFetchBlob from 'rn-fetch-blob';
import * as DocumentPicker from 'expo-document-picker';

var pathToken = RNFetchBlob.fs.dirs.DocumentDir + '/token.txt';

const instance = axios.create({
    baseURL: 'http://192.168.71.1:7012/',
})

function AddApp({ route, navigation }) {
    const [userId, setUserId] = useState(0);
    const [popularApp, setPopularApp] = useState([]);
    const [iconApp, setIconApp] = useState();
    const [wallpaperApp, setWallpaperApp] = useState();
    const [fileSetupApp, setFileSetupApp] = useState();

    const [nameApp, setNameApp] = useState('string');
    const [descriptionApp, setDescriptionApp] = useState('string');
    const [typeApp, setTypeApp] = useState('string');
    const [tagApp, setTagApp] = useState('string');

    const namePublisher = 'Riot';

    const selectIconFile = async () => {
        try {
            DocumentPicker.getDocumentAsync({
                type: '*/*',
            }).then(data => {
                if (data.type == 'cancel') {
                    console.log('cancel');
                }
                else {
                    setIconApp(data);
                    console.log(data);
                }
            });
        }
        catch (err) {
            throw err;
        }
    }

    const selectWallpaperFile = async () => {
        try {
            DocumentPicker.getDocumentAsync({
                type: '*/*',
            }).then(data => {
                if (data.type == 'cancel') {
                    console.log('cancel');
                }
                else {
                    setWallpaperApp(data);
                    console.log(data);
                }
            });
        }
        catch (err) {
            throw err;
        }
    }

    const selectFileSetup = async () => {
        try {
            DocumentPicker.getDocumentAsync({
                type: '*/*',
            }).then(data => {
                if (data.type == 'cancel') {
                    console.log('cancel');
                }
                else {
                    setFileSetupApp(data);
                    console.log(data);
                }
            });
        }
        catch (err) {
            throw err;
        }
    }

    function uploadApp() {
        if (iconApp == undefined) {
            console.log('set icon');
            return;
        }
        if (wallpaperApp == undefined) {
            console.log('set wallpaper');
            return;
        }
        if (fileSetupApp == undefined) {
            console.log('set fileSetup');
            return;
        }

        instance.post('api/App/create-app', {
            name: nameApp,
            description: descriptionApp,
            type: typeApp,
            publisher: 'Riot',
            tag: tagApp
        }).then((res) => {
            console.log(res.data);

            //upload icon
            RNFetchBlob.fetch('POST', 'http://192.168.71.1:7012/api/App/UploadIcon?id=' + res.data, {
                'Content-Type': 'multipart/form-data',
            }, [
                { name: 'fileUpload', filename: iconApp.name, type: '*/*', data: RNFetchBlob.wrap(iconApp.uri.replace('file://', '')) }
            ])
                .then((res) => {
                    // ...
                    console.log(res);
                }).catch((err) => {
                    // ...
                    console.log(err);
                })
            
            //upload wallpaper
            RNFetchBlob.fetch('POST', 'http://192.168.71.1:7012/api/App/UploadWallpaper?id=' + res.data, {
                'Content-Type': 'multipart/form-data',
            }, [
                { name: 'fileUpload', filename: wallpaperApp.name, type: '*/*', data: RNFetchBlob.wrap(wallpaperApp.uri.replace('file://', '')) }
            ])
                .then((res) => {
                    // ...
                    console.log(res);
                }).catch((err) => {
                    // ...
                    console.log(err);
                })
            
            //upload file setup
            RNFetchBlob.fetch('POST', 'http://192.168.71.1:7012/api/App/UploadFileSetup?id=' + res.data, {
                'Content-Type': 'multipart/form-data',
            }, [
                { name: 'fileUpload', filename: fileSetupApp.name, type: '*/*', data: RNFetchBlob.wrap(fileSetupApp.uri.replace('file://', '')) }
            ])
                .then((res) => {
                    // ...
                    console.log(res);
                }).catch((err) => {
                    // ...
                    console.log(err);
                })
            
            navigation.goBack();
        }).catch((err) => {
            console.log(err);
        })
        // RNFetchBlob.fetch('POST', 'http://192.168.0.104:7012/api/File/Upload', {
        //     'Content-Type': 'multipart/form-data',
        // }, [
        //     { name: 'fileUpload', filename: data.name, type: '*/*', data: RNFetchBlob.wrap(data.uri.replace('file://', '')) }
        // ])
        //     .then((res) => {
        //         // ...
        //         console.log(res);
        //     }).catch((err) => {
        //         // ...
        //         console.log(err);
        //     })
    }

    function renderHeader() {
        return (
            <View style={{ flex: 1, paddingHorizontal: 24, justifyContent: 'center' }}>
                <TouchableOpacity
                    style={{ width: 25, height: 25, margin: 12, position: 'absolute' }}
                    onPress={() => navigation.goBack()}
                >
                    <Image source={Icons.back_icon} resizeMode='center' style={{ width: 25, height: 25 }} />
                </TouchableOpacity>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={FONTS.h2}>Create App</Text>
                </View>
            </View>
        )
    }

    return (
        <SafeAreaView style={styles.AndroidSafeArea}>
            <View style={{ height: 60, }}>
                {renderHeader()}
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
                <TextInput
                    onChangeText={(text) => setNameApp(text)}
                    placeholder="Name" style={{ borderBottomWidth: 0.8, width: 280, height: 50, paddingLeft: 10 }} />
                <TextInput
                    onChangeText={(text) => setDescriptionApp(text)}
                    placeholder="Description" style={{ borderBottomWidth: 0.8, width: 280, height: 50, paddingLeft: 10, marginTop: 10 }} />
                <TextInput
                    onChangeText={(text) => setTypeApp(text)}
                    placeholder="Type" style={{ borderBottomWidth: 0.8, width: 280, height: 50, paddingLeft: 10, marginTop: 10 }} />
                <TextInput
                    onChangeText={(text) => setTagApp(text)}
                    placeholder="Tag" style={{ borderBottomWidth: 0.8, width: 280, height: 50, paddingLeft: 10, marginTop: 10 }} />
                <TouchableOpacity
                    onPress={() => selectIconFile()}
                    style={{ width: 280, height: 50, backgroundColor: COLORS.lightGreen1, borderRadius: 20, alignItems: 'center', justifyContent: 'center', marginTop: 20 }}
                >
                    <Text>Icon: {iconApp == undefined ? '' : iconApp.name}</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => selectWallpaperFile()}
                    style={{ width: 280, height: 50, backgroundColor: COLORS.lightGreen1, borderRadius: 20, alignItems: 'center', justifyContent: 'center', marginTop: 20 }}
                >
                    <Text>Wallpaper: {wallpaperApp == undefined ? '' : wallpaperApp.name}</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => selectFileSetup()}
                    style={{ width: 280, height: 50, backgroundColor: COLORS.lightGreen1, borderRadius: 20, alignItems: 'center', justifyContent: 'center', marginTop: 20 }}
                >
                    <Text>File Setup: {fileSetupApp == undefined ? '' : fileSetupApp.name}</Text>
                </TouchableOpacity>
            </View>
            <View>
                <TouchableOpacity
                    onPress={() => uploadApp()}
                    style={{ alignSelf: 'center', marginBottom: 24, width: 280, height: 50, backgroundColor: COLORS.lightGreen1, borderRadius: 20, alignItems: 'center', justifyContent: 'center', marginTop: 20 }}
                >
                    <Text>Upload App</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    )
}

export default AddApp;

const styles = StyleSheet.create({
    AndroidSafeArea: {
        flex: 1,
        backgroundColor: COLORS.base,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0
    },
    container: {
        flex: 1,
    },
});

const BlackMask = {
    backgroundColor: 'black',
    opacity: 0.1,
}

