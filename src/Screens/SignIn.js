import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, SafeAreaView, StatusBar, Image, TouchableOpacity, TextInput, KeyboardAvoidingView } from 'react-native';
import { FONTS, COLORS, Images, Icons } from "../Constants";
import axios from 'axios';
import RNFS from 'react-native-fs';
import RNFetchBlob from 'rn-fetch-blob';

const instance = axios.create({
    baseURL: 'http://192.168.71.1:7012/',
})

function SignIn({ navigation }) {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    function signIn() {
        instance.post('api/User/signin', {
            username: username,
            password: password,
            userType: 2
        }).then((res) => {
            console.log(res.data);
        }).catch((err) => {
            console.log(err);
        })

        navigation.goBack();
    }

    return (
        <KeyboardAvoidingView
            style={styles.container}
            behavior={Platform.OS === "ios" ? "padding" : "height"}
            enabled={false}
        >
            <SafeAreaView style={styles.AndroidSafeArea}>
                <View style={styles.container}>
                    <TouchableOpacity
                        style={{ position: 'absolute', width: 25, height: 25, margin: 12 }}
                        onPress={() => navigation.navigate("Welcome")}
                    >
                        <Image source={Icons.back_icon} resizeMode='center' style={{ width: 25, height: 25 }} />
                    </TouchableOpacity>

                    <View style={{ flex: 1.5, alignItems: 'center', justifyContent: 'flex-end' }}>
                        <Image source={Images.logo} resizeMode='cover' style={{ width: 200, height: 200 }} />
                        <Text style={{ ...FONTS.h1, fontWeight: 'bold', color: COLORS.lightGreen2 }}>Login</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <TextInput
                            onChangeText={(text) => setUsername(text)}
                            placeholder="Email" style={{ borderBottomWidth: 0.8, width: 280, height: 50, paddingLeft: 10 }} />
                        <TextInput
                            onChangeText={(text) => setPassword(text)}
                            placeholder="Password" style={{ borderBottomWidth: 0.8, width: 280, height: 50, paddingLeft: 10, marginTop: 10 }} />
                        <TextInput placeholder="ConfirmPassword" style={{ borderBottomWidth: 0.8, width: 280, height: 50, paddingLeft: 10, marginTop: 10 }} />
                    </View>
                    <View style={{ flex: 1, alignItems: 'center' }}>
                        <TouchableOpacity
                            style={{ backgroundColor: COLORS.lightGreen2, height: 50, width: 250, borderRadius: 30, marginTop: 12, justifyContent: 'center' }}
                            onPress={() => signIn()}
                        >
                            <Text style={{ ...FONTS.body2, alignSelf: 'center', color: COLORS.white, }}>SignIn</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>
        </KeyboardAvoidingView>
    )
}

export default SignIn;

const BlackMask = {
    backgroundColor: 'black',
    opacity: 0.1,
}

const styles = StyleSheet.create({
    AndroidSafeArea: {
        flex: 1,
        backgroundColor: COLORS.base,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0
    },
    container: {
        flex: 1,
    },
});
