import React, { useState, useEffect } from 'react';
import { COLORS, Images, FONTS, Icons } from "../Constants";
import { StyleSheet, Text, View, SafeAreaView, StatusBar, TouchableOpacity, Image, ScrollView, FlatList } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import axios from 'axios';
import RNFS from 'react-native-fs';
import RNFetchBlob from 'rn-fetch-blob';

const instance = axios.create({
    baseURL: 'http://192.168.71.1:7012/',
})

var pathToken = RNFetchBlob.fs.dirs.DocumentDir + '/token.txt';
let dirs = RNFetchBlob.fs.dirs.DownloadDir;

function AppDetail({ route, navigation }) {
    const [userId, setUserId] = useState(0);
    const [data, setData] = useState({});

    useEffect(() => {
        RNFS.readFile(pathToken, 'utf8').then((res) => {
            console.log(res);
            instance.get('api/App/get-app-by-id', {
                headers: {
                    Authorization: 'Bearer ' + res,
                },
                params: {
                    id: route.params.id,
                }
            }).then((res) => {
                // console.log(res.data);
                setData(res.data);
            }).catch((err) => {
                console.log(err);
            })
        })
        // instance.get('api/App/DownloadIcon?id=1').then((res) => {
        //     console.log(res);
        // }).catch((err) => {
        //     console.log(err);
        // })
    }, [userId]);

    function downloadFile() {
        RNFetchBlob
            .config({
                // add this option that makes response data to be stored as a file,
                // this is much more performant.
                fileCache: true,
                appendExt: 'apk',
                overwrite: true,
                // path: dirs,
            })
            .fetch('GET', 'http://192.168.71.1:7012/api/App/DownloadFileSetup?id=' + route.params.id, {
                //some headers ..
                // fileName: 'Meme 2.jpg'
            })
            .progress((received, total) => {
                console.log('progress', received / total)
            })
            .then((res) => {
                console.log(res.data);
                let filename = res.data.substring(res.data.lastIndexOf('/') + 1, res.data.length)
                console.log(filename);
                RNFS.moveFile(res.data, dirs + '/DoAn/' + filename).then(() => console.log('done'));
                // the temp file path
                console.log('The file saved to ', res.path());
                // res.flush();
            }).catch(error => console.log(error));
    }

    return (
        <SafeAreaView style={styles.AndroidSafeArea}>
            <View style={styles.container}>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 15, marginBottom: 12 }}>
                    <TouchableOpacity
                        onPress={() => navigation.goBack()}
                        style={{ width: 30, height: 30, marginLeft: 12, justifyContent: 'center', alignItems: 'center', }}
                    >
                        <Image source={Icons.back_icon} style={{ width: 25, height: 25, }} />
                    </TouchableOpacity>
                    <View style={{ flex: 1, alignItems: 'center' }}>
                        <Text style={{ fontSize: 20 }}>View Game</Text>
                    </View>
                    <TouchableOpacity style={{ width: 30, height: 30, marginRight: 15 }}>
                        <Image source={Images.more} style={{ width: 30, height: 30 }} />
                    </TouchableOpacity>
                </View>
                <ScrollView>
                    <View>
                        <View style={{ alignItems: 'center', marginTop: 12, marginHorizontal: 15, }}>
                            <Image
                                source={{ uri: 'http://192.168.71.1:7012/api/App/DownloadWallpaper?id=' + data.id }}
                                style={{ width: '100%', height: null, borderRadius: 50, aspectRatio: 1 }}
                            />
                        </View>
                        <View style={{ marginHorizontal: 15, marginTop: 24, flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                                <Text style={{ ...FONTS.h1, fontWeight: 'bold' }}>{data.name}</Text>
                                <Text style={{ ...FONTS.body3 }}>{data.publisher}</Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <Image source={Images.star}
                                    style={{
                                        width: 20,
                                        height: 20,
                                    }}
                                />
                                <Text style={{ marginLeft: 5, marginRight: 10, fontSize: 16 }}>(4.2)</Text>
                            </View>
                        </View>

                        <View style={{ alignItems: 'center', marginTop: 12 }}>
                            <TouchableOpacity
                                onPress={() => downloadFile()}
                                style={{ width: 300, height: 60, backgroundColor: COLORS.lightGreen1, borderRadius: 20, justifyContent: 'center', alignItems: 'center' }}
                            >
                                <Text style={{ ...FONTS.body2 }}>Download</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ marginHorizontal: 15, marginTop: 24 }}>
                            <Text style={{ ...FONTS.h2, fontSize: 24 }}>App Description</Text>
                            <Text style={{ ...FONTS.h4, }}>{data.description}</Text>
                        </View>
                    </View>
                </ScrollView>
            </View>
        </SafeAreaView>
    )
}

export default AppDetail;

const styles = StyleSheet.create({
    AndroidSafeArea: {
        flex: 1,
        backgroundColor: COLORS.base,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0
    },
    container: {
        flex: 1,
    },
});

const BlackMask = {
    backgroundColor: 'black',
    opacity: 0.1,
}