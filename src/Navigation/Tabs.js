import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Icons, COLORS } from "../Constants";
import { Home, Publisher } from "../Screens/";
import {
    Image
} from 'react-native';

const Tab = createBottomTabNavigator();

const tabOptions = {
    tabBarShowLabel: false,
    tabBarStyle: {
        height: "8%",
        backgroundColor: COLORS.lightGreen2,
    }
}

const Tabs = () => {
    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                ...tabOptions,
                tabBarIcon: ({ focused }) => {
                    const tintColor = focused ? COLORS.white : COLORS.gray;

                    switch (route.name) {
                        case "Home":
                            return (
                                <Image
                                    source={Icons.home_icon}
                                    resizeMode="contain"
                                    style={{
                                        tintColor: tintColor,
                                        width: 25,
                                        height: 25,
                                    }}
                                />
                            )

                        case "Search":
                            return (
                                <Image
                                    source={Icons.search_icon}
                                    resizeMode="contain"
                                    style={{
                                        tintColor: tintColor,
                                        width: 25,
                                        height: 25,
                                    }}
                                />
                            )

                        case "Notification":
                            return (
                                <Image
                                    source={Icons.notification_icon}
                                    resizeMode="contain"
                                    style={{
                                        tintColor: tintColor,
                                        width: 25,
                                        height: 25,
                                    }}
                                />
                            )

                        case "Setting":
                            return (
                                <Image
                                    source={Icons.setting_icon}
                                    resizeMode="contain"
                                    style={{
                                        tintColor: tintColor,
                                        width: 25,
                                        height: 25,
                                    }}
                                />
                            )

                        case "Publisher":
                            return (
                                <Image
                                    source={Icons.home_icon}
                                    resizeMode="contain"
                                    style={{
                                        tintColor: tintColor,
                                        width: 25,
                                        height: 25,
                                    }}
                                />
                            )
                    }
                }
            })}
        >
            <Tab.Screen
                name="Home"
                component={Home}
                options={{ headerShown: false }}
            />
            <Tab.Screen
                name="Publisher"
                component={Publisher}
                options={{ headerShown: false }}
            />
            {/* <Tab.Screen
                name="Search"
                component={Home}
                options={{ headerShown: false }}
            />
            <Tab.Screen
                name="Notification"
                component={Home}
                options={{ headerShown: false }}
            />
            <Tab.Screen
                name="Setting"
                component={Home}
                options={{ headerShown: false }}
            /> */}
        </Tab.Navigator>
    )
}

export default Tabs;