import React, { useState, useEffect } from 'react';
import { COLORS, Images, FONTS, Icons } from "../Constants";
import { StyleSheet, Text, View, SafeAreaView, StatusBar, TouchableOpacity, Image, ScrollView, FlatList } from 'react-native';
import RNFS from 'react-native-fs';
import RNFetchBlob from 'rn-fetch-blob';
import axios from 'axios';

const instance = axios.create({
    baseURL: 'http://192.168.71.1:7012/',
})

var pathToken = RNFetchBlob.fs.dirs.DocumentDir + '/token.txt';
var pathUsername = RNFetchBlob.fs.dirs.DocumentDir + '/username.txt';

function Home({ route, navigation }) {

    // const token = route.params.token;
    const [recommendedApp, setRecommendedApp] = useState([]);
    const [popularApp, setPopularApp] = useState([]);
    const [userId, setUserId] = useState(0);
    const [userName, setUserName] = useState('');

    useEffect(() => {
        RNFS.readFile(pathUsername, 'utf8').then((res) => {
            setUserName(res);
        })
        RNFS.readFile(pathToken, 'utf8').then((res) => {
            console.log(res);
            instance.get('api/App/get-all-app', {
                headers: {
                    Authorization: 'Bearer ' + res,
                }
            }).then((res) => {
                console.log(res.data);
                setPopularApp(res.data);
            }).catch((err) => {
                console.log(err);
            })

            instance.get('api/App/get-app-by-tag', {
                headers: {
                    Authorization: 'Bearer ' + res,
                },
                params: {
                    tag: 'Recommended',
                }
            }).then((res) => {
                // console.log(res.data);
                setRecommendedApp(res.data);
            }).catch((err) => {
                console.log(err);
            })
        })
    }, [userId]);

    function renderHeader(profile) {
        return (
            <View style={{ flex: 1, flexDirection: 'row', paddingHorizontal: 24, alignItems: 'center', }}>
                <View>
                    <Image source={Images.avatar} style={{ width: 50, height: 50, borderRadius: 25 }} />
                </View>

                <View style={{ flex: 1 }}>
                    <View style={{ marginHorizontal: 12 }}>
                        <Text style={{ ...FONTS.h2, color: COLORS.white }}>{userName}</Text>
                        <Text style={{ ...FONTS.h4, color: COLORS.white }}>Đồ án</Text>
                    </View>
                </View>

                <View>
                    <TouchableOpacity>
                        <Image source={Images.more} style={{ width: 30, height: 30 }} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    function renderCategory(categories) {

        const renderItem = ({ item, index }) => {
            return (
                <TouchableOpacity
                    style={{
                        flex: 1,
                        marginLeft: index == 0 ? 24 : 0,
                        marginRight: 12,
                        width: 140,
                        height: 140,
                        borderRadius: 30,
                        backgroundColor: COLORS.lightGreen2,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                // onPress={() => navigation.navigate("Home", {
                //     category: item
                // })}
                >
                    <View style={{ width: 80, height: 80, borderRadius: 40, alignItems: 'center', justifyContent: 'center', backgroundColor: COLORS.white }}>
                        <Image
                            source={Images.category}
                            resizeMode="cover"
                            style={{
                                width: 50,
                                height: 50,
                            }}
                        />
                    </View>

                    <Text style={{ ...FONTS.body3, color: COLORS.darkGreen }}>{item.categoryName}</Text>
                </TouchableOpacity>
            )
        }

        return (
            <View style={{ flex: 1 }}>
                <View style={{ paddingHorizontal: 24, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={{ ...FONTS.h2, color: COLORS.white }}>Category</Text>

                    <TouchableOpacity
                        onPress={() => console.log("See More")}
                        style={{ justifyContent: 'center' }}
                    >
                        <Text style={{ ...FONTS.body4, color: COLORS.black, alignSelf: 'flex-start' }}>See more</Text>
                    </TouchableOpacity>
                </View>

                {/* Categories */}
                <View style={{ flex: 1, marginTop: 12 }}>
                    <FlatList
                        data={categories}
                        renderItem={renderItem}
                        keyExtractor={item => `${item.id}`}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                    />
                </View>
            </View>
        )
    }

    function renderRecommended(recommendedData) {
        const renderItem = ({ item, index }) => {
            return (
                <TouchableOpacity
                    style={{
                        flex: 1,
                        marginLeft: index == 0 ? 24 : 0,
                        marginRight: 12,
                        width: 200,
                        height: 200,
                        borderRadius: 30,
                        backgroundColor: COLORS.lightGreen2,
                        justifyContent: 'center',
                    }}
                    onPress={() => console.log("Move to game detail")}
                >
                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <Image
                            source={{ uri: 'http://192.168.71.1:7012/api/App/DownloadWallpaper?id=' + item.id  }}
                            resizeMode="cover"
                            style={{
                                width: 180,
                                height: 120,
                                borderRadius: 20,
                            }}
                        />
                    </View>
                    <View style={{ flexDirection: 'row', marginHorizontal: 16, marginTop: 10 }}>
                        <View style={{ flex: 2, }}>
                            <Text numberOfLines={1} ellipsizeMode='tail' style={{ ...FONTS.body2, color: COLORS.darkGreen }}>{item.name}</Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Image source={Images.star} resizeMode='contain' style={{ width: 15, height: 15 }} />
                                <Text style={{ marginLeft: 5 }}>({item.rating})</Text>
                            </View>
                        </View>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <TouchableOpacity style={{ height: 30, width: 60, backgroundColor: COLORS.lightGreen1, justifyContent: 'center', alignItems: 'center', borderRadius: 10 }}>
                                <Text>Install</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                </TouchableOpacity>
            )
        }

        return (
            <View style={{ flex: 1 }}>
                <View style={{ paddingHorizontal: 24, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={{ ...FONTS.h2, color: COLORS.white }}>Recommended</Text>

                    <TouchableOpacity
                        onPress={() => console.log("See More")}
                        style={{ justifyContent: 'center' }}
                    >
                        <Text style={{ ...FONTS.body4, color: COLORS.black, alignSelf: 'flex-start' }}>See more</Text>
                    </TouchableOpacity>
                </View>

                {/* App */}
                <View style={{ flex: 1, marginTop: 12 }}>
                    <FlatList
                        data={recommendedData}
                        renderItem={renderItem}
                        keyExtractor={item => `${item.id}`}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                    />
                </View>
            </View>
        )
    }

    function renderPopularApp(popular) {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ paddingHorizontal: 24, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={{ ...FONTS.h2, color: COLORS.white }}>App</Text>

                    <TouchableOpacity
                        onPress={() => console.log("See More")}
                        style={{ justifyContent: 'center' }}
                    >
                        <Text style={{ ...FONTS.body4, color: COLORS.black, alignSelf: 'flex-start' }}>See more</Text>
                    </TouchableOpacity>
                </View>

                {/* App */}
                <View style={{ paddingHorizontal: 24, marginTop: 12 }}>
                    {popular.map((app) =>
                        <TouchableOpacity
                            onPress={() => navigation.navigate("AppDetail", {
                                id: app.id,
                            })}
                            style={{ flexDirection: 'row', paddingVertical: 5, }}
                            key={app.id}
                        >
                            <View style={{ flexDirection: 'row', flex: 1, }}>
                                <View style={{ width: 100, height: 100, borderRadius: 20, backgroundColor: COLORS.lightGreen2 }}>
                                    <Image
                                        // source={app.gameCover}
                                        source={{ uri: 'http://192.168.71.1:7012/api/App/DownloadIcon?id=' + app.id }}
                                        resizeMode="cover"
                                        style={{
                                            width: 100,
                                            height: 100,
                                            borderRadius: 20
                                        }}
                                    />
                                </View>
                                <View style={{ marginLeft: 10, marginVertical: 5 }}>
                                    <View style={{ flex: 1, justifyContent: 'flex-start' }}>
                                        <Text style={{ ...FONTS.body2, color: COLORS.black, alignSelf: 'flex-start' }}>{app.name}</Text>
                                        <Text style={{ ...FONTS.body4, color: COLORS.black, alignSelf: 'flex-start' }}>{app.publisher}</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', alignItems: 'flex-end', }}>
                                        <Image source={Images.star}
                                            style={{
                                                width: 18,
                                                height: 18,
                                            }}
                                        />
                                        <Text style={{ marginLeft: 5, marginRight: 10 }}>{app.rating}</Text>
                                        <Image source={Icons.download_icon}
                                            style={{
                                                width: 18,
                                                height: 18,
                                            }}
                                        />
                                        <Text style={{ marginLeft: 2, marginRight: 10 }}>{app.downloadCount}</Text>
                                        <View style={{ marginRight: 5, width: 8, height: 8, backgroundColor: COLORS.darkGreen, borderRadius: 4, alignSelf: 'center' }}></View>
                                        <Text>{app.type}</Text>
                                    </View>
                                </View>
                            </View>
                        </TouchableOpacity>
                    )}
                </View>
                {/* <View style={{ flex: 1, marginTop: 12 }}>
                    <FlatList
                        data={recommendedData}
                        renderItem={renderItem}
                        keyExtractor={item => `${item.id}`}
                    />
                </View> */}
            </View>
        )
    }

    return (
        <SafeAreaView style={styles.AndroidSafeArea}>
            <View style={styles.container}>
                <View style={{ height: 60, }}>
                    {renderHeader(profileData)}
                </View>

                <ScrollView style={{ marginTop: 12 }}>
                    <View>
                        {renderCategory(categories)}
                    </View>

                    <View style={{ marginTop: 12 }}>
                        <View>
                            {renderRecommended(recommendedApp)}
                        </View>
                    </View>

                    <View style={{ marginTop: 12 }}>
                        <View>
                            {renderPopularApp(popularApp)}
                        </View>
                    </View>
                </ScrollView>
            </View>
        </SafeAreaView>
    )
}

export default Home;

const BlackMask = {
    backgroundColor: 'black',
    opacity: 0.1,
}

const styles = StyleSheet.create({
    AndroidSafeArea: {
        flex: 1,
        backgroundColor: COLORS.base,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0
    },
    container: {
        flex: 1,
    },
});

const profileData = {
    name: 'Niletsnug',
}

const categories = [
    {
        id: 1,
        categoryName: "Action",
    },
    {
        id: 2,
        categoryName: "Adventure",
    },
    {
        id: 3,
        categoryName: "Casual",
    },
    {
        id: 4,
        categoryName: "Open World",
    },
]

const gameValorant = {
    id: 1,
    name: "Valorant",
    gameCover: Images.valorantWallpaper,
    rate: 4.8,
}

const gameGenshinImpact = {
    id: 2,
    name: "Genshin Impact",
    gameCover: Images.genshinImpactWallpaper,
    rate: 4.8,
}

const recommendedData = [
    {
        ...gameValorant,
    },
    {
        ...gameGenshinImpact,
    },
]