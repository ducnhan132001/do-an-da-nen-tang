import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, SafeAreaView, StatusBar, Image, TouchableOpacity, TextInput, KeyboardAvoidingView, Alert } from 'react-native';
import { FONTS, COLORS, Images, Icons } from "../Constants";
import axios from 'axios';
import RNFS from 'react-native-fs';
import RNFetchBlob from 'rn-fetch-blob';

const instance = axios.create({
    baseURL: 'http://192.168.71.1:7012/',
})

var pathToken = RNFetchBlob.fs.dirs.DocumentDir + '/token.txt';
var pathUsername = RNFetchBlob.fs.dirs.DocumentDir + '/username.txt';

function Login({ navigation }) {

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    function Login() {
        instance.post('/api/User/login', {
            username: username,
            password: password
        }).then((res) => {
            console.log(res.data);
            RNFS.writeFile(pathToken, res.data.token, 'utf8')
                .then((success) => {
                    console.log('FILE WRITTEN!');
                })
                .catch((err) => {
                    console.log(err.message);
                });
            RNFS.writeFile(pathUsername, username, 'utf8')
                .then((success) => {
                    console.log('FILE WRITTEN!');
                })
                .catch((err) => {
                    console.log(err.message);
                });
            navigation.navigate('Tab', {
                token: res.data.token,
            })
        }).catch((err) => {
            console.log(err);
            Alert.alert(
                "Wrong username or password",
                "Wrong username or password",
                [
                    {
                        text: "Cancel",
                        onPress: () => console.log("Cancel Pressed"),
                        style: "cancel"
                    },
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ]
            );
        })
    }

    return (
        <KeyboardAvoidingView
            style={styles.container}
            behavior={Platform.OS === "ios" ? "padding" : "height"}
            enabled={false}
        >
            <SafeAreaView style={styles.AndroidSafeArea}>
                <View style={styles.container}>
                    <TouchableOpacity
                        style={{ position: 'absolute', width: 25, height: 25, margin: 12 }}
                        onPress={() => navigation.goBack()}
                    >
                        <Image source={Icons.back_icon} resizeMode='center' style={{ width: 25, height: 25 }} />
                    </TouchableOpacity>

                    <View style={{ flex: 1.5, alignItems: 'center', justifyContent: 'flex-end' }}>
                        <Image source={Images.logo} resizeMode='cover' style={{ width: 200, height: 200 }} />
                        <Text style={{ ...FONTS.h1, fontWeight: 'bold', color: COLORS.lightGreen2 }}>Login</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <TextInput
                            onChangeText={(text) => setUsername(text)}
                            placeholder="Email" style={{ borderBottomWidth: 0.8, width: 280, height: 50, paddingLeft: 10 }} />
                        <TextInput
                            onChangeText={(text) => setPassword(text)}
                            placeholder="Password" style={{ borderBottomWidth: 0.8, width: 280, height: 50, paddingLeft: 10, marginTop: 10 }} />
                    </View>
                    <View style={{ flex: 1, alignItems: 'center' }}>
                        <TouchableOpacity
                            style={{ backgroundColor: COLORS.lightGreen2, height: 50, width: 250, borderRadius: 30, marginTop: 12, justifyContent: 'center' }}
                            onPress={() => Login()}
                        >
                            <Text style={{ ...FONTS.body2, alignSelf: 'center', color: COLORS.white, }}>Login</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>
        </KeyboardAvoidingView>
    )
}

export default Login;

const BlackMask = {
    backgroundColor: 'black',
    opacity: 0.1,
}

const styles = StyleSheet.create({
    AndroidSafeArea: {
        flex: 1,
        backgroundColor: COLORS.base,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0
    },
    container: {
        flex: 1,
    },
});
