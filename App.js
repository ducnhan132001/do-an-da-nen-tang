import { StatusBar } from 'expo-status-bar';
import React from 'react';
import Tabs from "./src/Navigation/Tabs";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import { Welcome, Login, SignIn, TestScreen, AppDetail, Publisher, AddApp } from './src/Screens';
import {
	StyleSheet,
	Text,
	View,
	SafeAreaView
} from 'react-native';

const Stack = createNativeStackNavigator();

export default function App() {
	return (
		<NavigationContainer>
            <Stack.Navigator
                screenOptions={{
                    headerShown: false
                }}
                initialRouteName={"Welcome"}
            >
                {/* Test */}
                <Stack.Screen name="Test" component={TestScreen} options={{ headerShown: false }}/>
                {/* Tabs */}
                <Stack.Screen name="Tab" component={Tabs} options={{ headerShown: false }}/>
                {/* Screens */}
				<Stack.Screen name="Welcome" component={Welcome} options={{ headerShown: false }} />
				<Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
                <Stack.Screen name="SignIn" component={SignIn} options={{ headerShown: false }} />
                <Stack.Screen name="AppDetail" component={AppDetail} options={{ headerShown: false }} />
                <Stack.Screen name="Publisher" component={Publisher} options={{ headerShown: false }} />
                <Stack.Screen name="AddApp" component={AddApp} options={{ headerShown: false }} />
            </Stack.Navigator>
        </NavigationContainer>
	);
}
