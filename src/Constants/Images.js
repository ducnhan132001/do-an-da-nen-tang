export const logo = require("../Assets/logo.png");
export const avatar = require("../Assets/avatar.png");
export const more = require("../Assets/more.png");
export const category = require("../Assets/category.png");
export const valorantWallpaper = require("../Assets/valorant_wallpaper.png");
export const genshinImpactWallpaper = require("../Assets/genshinimapct_wallpaper.png");
export const star = require("../Assets/star.png");

export default {
    logo, avatar, more, category, valorantWallpaper, star, genshinImpactWallpaper
}