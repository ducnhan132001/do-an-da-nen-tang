export const home_icon = require("../Assets/home_icon.png");
export const search_icon = require("../Assets/search_icon.png");
export const notification_icon = require("../Assets/notification_icon.png");
export const setting_icon = require("../Assets/setting_icon.png");
export const back_icon = require("../Assets/back_icon.png");
export const download_icon = require("../Assets/download_icon.png");
export const add_icon = require("../Assets/add_icon.png")

export default {
    home_icon,
    search_icon,
    notification_icon,
    setting_icon,
    back_icon,
    download_icon,
    add_icon,
}