import React, { useState, useEffect } from 'react';
import { COLORS, Images, FONTS, Icons } from "../Constants";
import { StyleSheet, Text, View, SafeAreaView, StatusBar, TouchableOpacity, Image, ScrollView, FlatList } from 'react-native';
import axios from 'axios';
// import DocumentPicker, {
//     DirectoryPickerResponse,
//     DocumentPickerResponse,
//     isInProgress,
//     types,
// } from 'react-native-document-picker';
import * as DocumentPicker from 'expo-document-picker';
import RNFetchBlob from 'rn-fetch-blob';
import RNFS from 'react-native-fs';

const instance = axios.create({
    baseURL: 'http://192.168.0.104:7012',
})

function TestScreen() {
    let dirs = RNFetchBlob.fs.dirs.DownloadDir;
    console.log(dirs);

    const selectOneFile = async () => {
        try {
            DocumentPicker.getDocumentAsync({
                type: '*/*',
            }).then(data => {
                if (data.type == 'cancel') {
                    console.log('cancel');
                }
                else {
                    let formData = new FormData();
                    formData.append("file", data);

                    RNFetchBlob.fetch('POST', 'http://192.168.0.104:7012/api/File/Upload', {
                        'Content-Type': 'multipart/form-data',
                    }, [
                        { name: 'fileUpload', filename: data.name, type: '*/*', data: RNFetchBlob.wrap(data.uri.replace('file://', '')) }
                    ])
                        .then((res) => {
                            // ...
                            console.log(res);
                        }).catch((err) => {
                            // ...
                            console.log(err);
                        })
                }
            });
        }
        catch (err) {
            throw err;
        }
    }

    // const finished = promisify(stream.finished);
    const fileName = 'test.apk'

    function downloadFile() {
        RNFetchBlob
            .config({
                // add this option that makes response data to be stored as a file,
                // this is much more performant.
                fileCache: true,
                appendExt: 'apk',
                overwrite: true,
                // path: dirs,
            })
            .fetch('GET', 'http://192.168.0.104:7012/api/File/DownloadFile?fileName=' + fileName, {
                //some headers ..
                // fileName: 'Meme 2.jpg'
            })
            .progress((received, total) => {
                console.log('progress', received / total)
            })
            .then((res) => {
                console.log(res.data);
                let filename = res.data.substring(res.data.lastIndexOf('/') + 1, res.data.length)
                console.log(filename);
                RNFS.moveFile(res.data, dirs + '/DoAn/' + filename).then(() => console.log('done'));
                // the temp file path
                console.log('The file saved to ', res.path());
                // res.flush();
            }).catch(error => console.log(error));

        // RNFetchBlob.fs.unlink(dirsTwo + '/Test').then(() => {
        //     console.log('unlink');
        // })
    }

    function get() {
        instance.get('api/Upload/get-test', {
            timeout: 5000,
        }).then(res => {
            // console.log(res);
            console.log(res.data)
        })
            .catch(error => console.log(error));
    }

    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
            <TouchableOpacity
                // onPress={() => selectOneFile()}
                onPress={() => downloadFile()}
                style={{ backgroundColor: '#8ee86b', height: 60, width: 200, alignItems: 'center', justifyContent: 'center', borderRadius: 40 }}
            >
                <Text>Download</Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={() => selectOneFile()}
                // onPress={() => downloadFile()}
                style={{ backgroundColor: '#8ee86b', marginTop: 12, height: 60, width: 200, alignItems: 'center', justifyContent: 'center', borderRadius: 40 }}
            >
                <Text>Select File</Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={() => downloadFileUseFs()}
                // onPress={() => downloadFile()}
                style={{ backgroundColor: '#8ee86b', marginTop: 12, height: 60, width: 200, alignItems: 'center', justifyContent: 'center', borderRadius: 40 }}
            >
                <Text>Download File Axios</Text>
            </TouchableOpacity>
        </View>
    )
}

export default TestScreen;