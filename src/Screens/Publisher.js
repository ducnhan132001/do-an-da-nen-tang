import React, { useState, useEffect } from 'react';
import { COLORS, Images, FONTS, Icons } from "../Constants";
import { StyleSheet, Text, View, SafeAreaView, StatusBar, TouchableOpacity, Image, ScrollView, FlatList } from 'react-native';
import axios from 'axios';
import RNFS from 'react-native-fs';
import RNFetchBlob from 'rn-fetch-blob';

var pathToken = RNFetchBlob.fs.dirs.DocumentDir + '/token.txt';
var pathUsername = RNFetchBlob.fs.dirs.DocumentDir + '/username.txt';

const instance = axios.create({
    baseURL: 'http://192.168.71.1:7012/',
})

function Publisher({ navigation }) {
    const [userId, setUserId] = useState(0);
    const [popularApp, setPopularApp] = useState([]);
    const [userName, setUserName] = useState('');
    const [userType, setUserType] = useState(2);

    useEffect(() => {
        RNFS.readFile(pathUsername, 'utf8').then((resUsername) => {
            setUserName(resUsername);
            RNFS.readFile(pathToken, 'utf8').then((res) => {
                console.log(res);

                instance.get('api/User/get-user-type', {
                    headers: {
                        Authorization: 'Bearer ' + res,
                    },
                    params: {
                        username: resUsername
                    }
                }).then((res) => {
                    console.log(res.data);
                    setUserType(res.data);
                }).catch((err) => {
                    console.log(err);
                })

                instance.get('api/App/get-app-by-publisher', {
                    headers: {
                        Authorization: 'Bearer ' + res,
                    },
                    params: {
                        publisher: userName
                    }
                }).then((res) => {
                    console.log(res.data);
                    setPopularApp(res.data);
                }).catch((err) => {
                    console.log(err);
                })
            })



            // instance.get('api/App/get-app-by-publisher', {
            //     headers: {
            //         Authorization: 'Bearer ' + res,
            //     },
            //     params: {
            //         publisher: 'Riot',
            //     }
            // }).then((res) => {
            //     // console.log(res.data);
            //     setPopularApp(res.data);
            // }).catch((err) => {
            //     console.log(err);
            // })
        })
    }, [userId]);

    function renderPopularApp(popular) {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ paddingHorizontal: 24, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={{ ...FONTS.h2, color: COLORS.white }}>App</Text>

                    <TouchableOpacity
                        onPress={() => console.log("See More")}
                        style={{ justifyContent: 'center' }}
                    >
                        <Text style={{ ...FONTS.body4, color: COLORS.black, alignSelf: 'flex-start' }}>See more</Text>
                    </TouchableOpacity>
                </View>

                {/* App */}
                <View style={{ paddingHorizontal: 24, marginTop: 12 }}>
                    {popular.map((app) =>
                        <TouchableOpacity
                            onPress={() => navigation.navigate("AppDetail", {
                                id: app.id,
                            })}
                            style={{ flexDirection: 'row', paddingVertical: 5, }}
                            key={app.id}
                        >
                            <View style={{ flexDirection: 'row', flex: 1, }}>
                                <View style={{ width: 100, height: 100, borderRadius: 20, backgroundColor: COLORS.lightGreen2 }}>
                                    <Image
                                        // source={app.gameCover}
                                        source={{ uri: 'http://192.168.71.1:7012/api/App/DownloadIcon?id=' + app.id }}
                                        resizeMode="cover"
                                        style={{
                                            width: 100,
                                            height: 100,
                                            borderRadius: 20
                                        }}
                                    />
                                </View>
                                <View style={{ marginLeft: 10, marginVertical: 5 }}>
                                    <View style={{ flex: 1, justifyContent: 'flex-start' }}>
                                        <Text style={{ ...FONTS.body2, color: COLORS.black, alignSelf: 'flex-start' }}>{app.name}</Text>
                                        <Text style={{ ...FONTS.body4, color: COLORS.black, alignSelf: 'flex-start' }}>{app.publisher}</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', alignItems: 'flex-end', }}>
                                        <Image source={Images.star}
                                            style={{
                                                width: 18,
                                                height: 18,
                                            }}
                                        />
                                        <Text style={{ marginLeft: 5, marginRight: 10 }}>{app.rating}</Text>
                                        <Image source={Icons.download_icon}
                                            style={{
                                                width: 18,
                                                height: 18,
                                            }}
                                        />
                                        <Text style={{ marginLeft: 2, marginRight: 10 }}>{app.downloadCount}</Text>
                                        <View style={{ marginRight: 5, width: 8, height: 8, backgroundColor: COLORS.darkGreen, borderRadius: 4, alignSelf: 'center' }}></View>
                                        <Text>{app.type}</Text>
                                    </View>
                                </View>
                            </View>
                        </TouchableOpacity>
                    )}
                </View>
            </View>
        )
    }

    function renderHeader(profile) {
        return (
            <View style={{ flex: 1, flexDirection: 'row', paddingHorizontal: 24, alignItems: 'center', }}>
                <View>
                    <Image source={Images.avatar} style={{ width: 50, height: 50, borderRadius: 25 }} />
                </View>

                <View style={{ flex: 1 }}>
                    <View style={{ marginHorizontal: 12 }}>
                        <Text style={{ ...FONTS.h2, color: COLORS.white }}>{userName}</Text>
                        <Text style={{ ...FONTS.h4, color: COLORS.white }}>Đồ án</Text>
                    </View>
                </View>

                <View>
                    <TouchableOpacity
                        onPress={() => navigation.navigate('AddApp')}
                    >
                        <Image source={Icons.add_icon} style={{ width: 30, height: 30, opacity: 0.5 }} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    
    function reload() {
        RNFS.readFile(pathUsername, 'utf8').then((resUsername) => {
            setUserName(resUsername);
            RNFS.readFile(pathToken, 'utf8').then((res) => {
                console.log(res);

                instance.get('api/User/get-user-type', {
                    headers: {
                        Authorization: 'Bearer ' + res,
                    },
                    params: {
                        username: resUsername
                    }
                }).then((res) => {
                    console.log(res.data);
                    setUserType(res.data);
                }).catch((err) => {
                    console.log(err);
                })

                instance.get('api/App/get-app-by-publisher', {
                    headers: {
                        Authorization: 'Bearer ' + res,
                    },
                    params: {
                        publisher: userName
                    }
                }).then((res) => {
                    console.log(res.data);
                    setPopularApp(res.data);
                }).catch((err) => {
                    console.log(err);
                })
            })
        })
    }

    function becomePublisher() {
        RNFS.readFile(pathToken, 'utf8').then((res) => {
            instance.post('api/User/become-publisher', {
                userName: userName
            }, {
                headers: {
                    Authorization: 'Bearer ' + res,
                },
                params: {
                    username: userName
                }
            }).then((res) => {
                console.log(res.data);
                // navigation.goBack();
                reload();
            }).catch((err) => {
                console.log(err);
            })
        })
    }

    function renderPublisher() {
        if (userType == '1') {
            return (
                <View>
                    <View style={{ height: 60, }}>
                        {renderHeader(profileData)}
                    </View>
                    <ScrollView style={{ marginTop: 12, }}>
                        <View style={{ marginTop: 12 }}>
                            <View>
                                {renderPopularApp(popularApp)}
                            </View>
                        </View>
                    </ScrollView>
                </View>
            )
        }
        else {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity
                        onPress={() => becomePublisher()}
                        style={{ width: 100, height: 60, borderRadius: 20, backgroundColor: COLORS.lightGreen1, justifyContent: 'center', alignItems: 'center' }}
                    >
                        <Text>Become Publisher</Text>
                    </TouchableOpacity>
                </View>
            )
        }
    }

    return (
        <SafeAreaView style={styles.AndroidSafeArea}>
            {renderPublisher()}
        </SafeAreaView>
    )
}

export default Publisher;

const styles = StyleSheet.create({
    AndroidSafeArea: {
        flex: 1,
        backgroundColor: COLORS.base,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0
    },
    container: {
        flex: 1,
    },
});

const BlackMask = {
    backgroundColor: 'black',
    opacity: 0.1,
}

const profileData = {
    name: 'Niletsnug',
}