import React from 'react';
import { StyleSheet, Text, View, SafeAreaView, StatusBar, Image, TouchableOpacity } from 'react-native';
import { FONTS, COLORS, Images } from "../Constants";

function Welcome({ navigation }) {
    return (
        <SafeAreaView style={styles.AndroidSafeArea}>
            <View style={styles.container}>
                <View style={{ flex: 2, alignItems: 'center', justifyContent: 'flex-end' }}>
                    <Image source={Images.logo} resizeMode='cover' style={{ width: 200, height: 200 }} />
                    <Text style={{ ...FONTS.h1, fontWeight: 'bold', color: COLORS.lightGreen2 }}>Welcome</Text>
                </View>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity
                        style={{ backgroundColor: COLORS.lightGreen2, height: 50, width: 250, borderRadius: 30, justifyContent: 'center' }}
                        onPress={() => navigation.navigate("Login")}
                    >
                        <Text style={{ ...FONTS.body2, alignSelf: 'center', color: COLORS.white, }}>LOGIN</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{ backgroundColor: COLORS.lightGreen2, height: 50, width: 250, borderRadius: 30, marginTop: 12, justifyContent: 'center' }}
                        onPress={() => navigation.navigate("SignIn")}
                    >
                        <Text style={{ ...FONTS.body2, alignSelf: 'center', color: COLORS.white, }}>SIGN IN</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ flex: 1, }}>

                </View>
            </View>
        </SafeAreaView>
    )
}

export default Welcome;

const styles = StyleSheet.create({
    AndroidSafeArea: {
        flex: 1,
        backgroundColor: COLORS.base,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0
    },
	container: {
        flex: 1,
    },
});
